#include <stdio.h>
#include <stdlib.h>

#define BLOCKS_PER_GRID 101
#define THREADS_PER_BLOCK 1024

// matrix func
typedef struct {
	int height;
	int width;
	int *elements;
} Matrix;

// forward declaration
void print_data(int*, int, int);

// kernel version 1
__global__ void kernel(int *d_data, unsigned int width) {
	int i;
	unsigned idx = threadIdx.x + blockIdx.x * blockDim.x;

	// testing for thread 1 (idx)
	if(idx == 401) {
	printf("\n\nThreadID = %d\n\n", idx, d_data[idx]);

	for(i = 0; i < width; i++) {
		printf("d_data[%d] = %d,\t", idx * width + i, d_data[idx * width + i]);

		if(i != 0) {
			d_data[idx * width] = d_data[idx * width] + d_data[idx * width + i];
			printf("-> %d = %d + %d\n", d_data[idx * width], d_data[idx * width], d_data[idx * width + i]);
		}
	}

	printf("\nd_data[%d] = %d\n", idx * width, d_data[idx * width]);
	__syncthreads();	
	}
}

// kernel version 2
__global__ void kernelv2(int *d_data, int width) {
	int f;
	int idx = threadIdx.x + blockIdx.x * blockDim.x;

	if(idx < 102540) {
	
		for(f = 0; f < width; f++) {
			if( f != 0)
				d_data[idx * width] = d_data[idx * width] + d_data[idx * width + f];
		}

		__syncthreads();
	}
}

// main **
int main() {
	int e, f;
	cudaError_t error;

	// matrix definition
	Matrix h_data;
	int *d_data;

	h_data.height = 102540; h_data.width = 10;
	int size = h_data.height * h_data.width;
	h_data.elements = (int*) calloc(size, sizeof(int));

	// put dummy values in matrix/vector
	for(e = 0; e < h_data.height; e++) {
		for(f = 0; f < h_data.width; f++) {
			h_data.elements[e * h_data.width + f] = 1;//(e * h_data.width + f);
			//printf("%d ", h_data.elements[e * h_data.width + f]);
		}
	}

	// device mem alloc
	size_t cudaSize = h_data.width * h_data.height * sizeof(int);
	error = cudaMalloc(&d_data, cudaSize);

	if(error != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed: %s\n", cudaGetErrorString(error));
		exit(1);
	}

	// copy host to device
	error = cudaMemcpy(d_data, h_data.elements, cudaSize, cudaMemcpyHostToDevice);

	if(error != cudaSuccess) {
		fprintf(stderr, "cudaMemcpyDeviceToHost failed: %s\n", cudaGetErrorString(error));
		exit(1);
	}

	// kernel launches
	kernelv2<<< BLOCKS_PER_GRID, THREADS_PER_BLOCK >>>(d_data, h_data.width);

	// copy device to host
	error = cudaMemcpy(h_data.elements, d_data, cudaSize, cudaMemcpyDeviceToHost);

	if(error != cudaSuccess) {
		fprintf(stderr, "cudaMemcpyDeviceToHost failed: %s\n", cudaGetErrorString(error));
		exit(1);
	}

	// printout to text file **
	print_data(h_data.elements, h_data.height, h_data.width);

	// clean up memory
	error = cudaFree(d_data);
	
	if(error != cudaSuccess) {
		fprintf(stderr, "cudaFree failed: %s\n", cudaGetErrorString(error));
		exit(1);
	}

	return 0;
}

// print result to text file **
void print_data(int *data, int height, int width) {

	FILE *file;

	file = fopen("result.txt", "w+");

	if(file == NULL) {
		fprintf(stderr, "Failed writting to result.txt file\n");
		exit(1);

	} else {
		int e;
		for(e = 0; e < height; e++) {
			fprintf(file, "%d\n", data[e * width]);
		}
	}

	fclose(file);
}
