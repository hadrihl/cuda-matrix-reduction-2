CUDA = nvcc
CUDA_FLAGS = -arch=sm_20 -m64 -G -g

BIN = cuda-matrix-reduction-2

all: $(BIN)

cuda-matrix-reduction-2: main.cu
	$(CUDA) $(CUDA_FLAGS) -o $@ $<

clean:
	$(RM) $(BIN) result.txt
